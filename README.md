# ToDo app React Native

In deze README beschrijf ik hoe je de app kan bekijken en welke stappen hier voor nodig zijn.
Ik heb gebruik gemaakt van expo, deze maakt het maken van react native apps een stuk makkelijker en geeft je prettige tools voor ontwikkelen.

Bij deze app hoef je niet in te loggen, aangezien de data alleen op je telefoon wordt opgeslagen.

Je kan gemakkelijk nieuwe to-do's aanmaken, en ze worden automatisch opgeslagen lokaal.

## Pre requirements

Zorg dat je [node](https://nodejs.org/en/) hebt geinstalleerd op je machine. Tijdens het ontwikkel proces heb ik gebruik gemaakt van v10.16.3
Het is aan te raden om niet een veel oudere of nieuwere versie als mijn versie te gebruiken. Dit kan voor problemen zorgen.

Ook helpt het om een goede code editer te gebruiken, ik kan [VSCode](https://code.visualstudio.com/) aanraden.

## Installatie

Clone of download dit project en gebruik vervolgens `npm install` om alle packages te installeren.

## Applicatie starten

Gebruik `npm run start` om het start script te starten.
Er wordt een tab in je browser geopent met een wat informatie en een QR code.

Je krijgt het volgende expo scherm te zien:
![Expo](./assets/ss-expo.png)

Kies je onderstaande methode uit om de applicatie te bekijken.
Alle 3 de opties beschikken over hot-reloading, waardoor het development proces erg fijn en makkelijk is!

### web

Klik op de `Run in web browser` om de React Native app in de browser te kunnen bekijken

### Android

Download de `expo` app in de google app store, open de app en scan de QR code die je krijgt als je `npm run start` hebt gedraaid.

### iOs

Voor iOs heb je een [expo account](https://expo.io/signup) nodig en de expo app uit de app store.
Als je een account hebt aangemaakt kan je inloggen bij de iOs app en dan kan je vervolgens de link naar je iOs apparaat sturen.
Dit kan je doen door in de website die opende bij het draaien van `npm run start` te klikken op `send link with email`. Open deze link op je iOs device (iphone of ipad) en je krijgt een pop-up om deze te openen met de expo app. Doe dat je zou nu de app moeten zien.

## Gebruik van de app

De app maakt gebruik van persistent storage, waardoor je de app kan afsluiten en vervolgens je todo's correct bewaard blijven. Je kan op de knop `voeg toe` drukken om een nieuwe todo item toe te voegen.

De app maakt gebruik van redux voor state management (overkill, maar wilde het laten zien), bij het opstarten van de app wordt er gekeken of er in de persistent storage wat staat, als dat zo is voegt die het toe aan de redux store waardoor je je todo's goed te zien krijgt.
