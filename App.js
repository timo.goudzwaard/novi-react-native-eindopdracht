import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { Provider } from 'react-redux';
import CreateToDoScreen from './src/screens/CreateToDoScreen';
import HomeScreen from './src/screens/HomeScreen';
import store from './src/state/store';

const Stack = createStackNavigator();

const App = () => {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="Welkom bij To-Do app" component={HomeScreen} />
          <Stack.Screen name="Create To-Do" component={CreateToDoScreen} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
};

export default App;
