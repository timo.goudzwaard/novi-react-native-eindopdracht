import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Text, View, StyleSheet, Button } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import { useDispatch } from 'react-redux';
import { Card } from '../components/card/card';
import { addTodo } from '../state/actions';

const styles = StyleSheet.create({
  title: {
    fontSize: 20,
    textAlign: 'center',
  },
  paragraph: {
    marginTop: 12,
    marginBottom: 12,
    textAlign: 'center',
  },
  input: {
    backgroundColor: '#fff',
    borderRadius: 3,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    padding: 4,
    marginBottom: 12,
    marginTop: 20,
  },
  container: {
    padding: 20,
  },
});

const CreateToDoScreen = ({ navigation }) => {
  const dispatch = useDispatch();
  const [newTodo, setNewTodo] = useState('');

  const handleAddTodo = () => {
    dispatch(addTodo(newTodo));
    setNewTodo('');
    navigation.navigate('Welkom bij To-Do app');
  };

  return (
    <View style={styles.container}>
      <Card>
        <Text style={styles.title}>Create To Do Item</Text>
        <TextInput
          value={newTodo}
          onChangeText={(value) => {
            setNewTodo(value);
          }}
          autoFocus
          style={styles.input}
          onSubmitEditing={() => {
            handleAddTodo();
          }}
        />
        <Button
          title="Voeg toe"
          onPress={() => {
            handleAddTodo();
          }}
        />
      </Card>
    </View>
  );
};

CreateToDoScreen.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
};

export default CreateToDoScreen;
