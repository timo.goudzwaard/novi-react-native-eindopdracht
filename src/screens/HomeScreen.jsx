import PropTypes from 'prop-types';
import React from 'react';
import { Text, View, Button, StyleSheet } from 'react-native';
import { useSelector } from 'react-redux';
import { Card } from '../components/card/card';
import TodoItem from '../components/todo-item/todo-item';

const styles = StyleSheet.create({
  title: {
    fontSize: 20,
    textAlign: 'center',
  },
  paragraph: {
    marginTop: 12,
    marginBottom: 12,
    textAlign: 'center',
  },
  container: {
    padding: 20,
  },
  todos: {
    marginTop: 20,
    marginBottom: 20,
  },
  information: {
    textAlign: 'center',
  },
});

const Home = ({ navigation }) => {
  const todos = useSelector((state) => state);

  const renderTodos = () => {
    if (todos.length === 0) {
      return (
        <Text style={styles.information}>
          Je hebt nog geen todo items, voeg er 1 toe om te starten.
        </Text>
      );
    }

    return todos.map((todo) => {
      return <TodoItem key={todo} item={todo} />;
    });
  };

  return (
    <View style={styles.container}>
      <Card>
        <Text style={styles.title}>To-Do</Text>
        <View style={styles.todos}>{renderTodos()}</View>
        <Button
          title="Voeg toe"
          onPress={() => {
            navigation.navigate('Create To-Do');
          }}
        />
      </Card>
    </View>
  );
};

Home.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
};

export default Home;
