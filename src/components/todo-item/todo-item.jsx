import React from 'react';
import { View, Text, StyleSheet, Button } from 'react-native';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { removeTodo } from '../../state/actions';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 4,
    marginBottom: 4,
  },
  button: {
    fontSize: 20,
  },
  text: {
    fontWeight: '600',
  },
});

const TodoItem = ({ item }) => {
  const dispatch = useDispatch();

  const handleRemove = () => {
    dispatch(removeTodo(item));
  };

  return (
    <View style={styles.container}>
      <Text style={styles.text}>{item}</Text>
      <Button
        style={styles.button}
        title="✓"
        onPress={() => {
          handleRemove();
        }}
      />
    </View>
  );
};

TodoItem.propTypes = {
  item: PropTypes.string.isRequired,
};

export default TodoItem;
