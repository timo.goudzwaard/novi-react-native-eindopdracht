import { configureStore, createReducer } from '@reduxjs/toolkit';
import getData from './storage/getData';
import { addTodo, removeTodo } from './actions';
import storeData from './storage/storeData';

const todo = createReducer([], {
  [addTodo]: (state, action) => {
    // only add todo if it doesn't exist already
    if (!state.includes(action.payload)) {
      state.push(action.payload);
    }

    storeData(state);

    return state;
  },
  [removeTodo]: (state, action) => {
    const newState = state.filter((item) => item !== action.payload);

    storeData(newState);

    return newState;
  },
});

const store = configureStore({
  reducer: todo,
});

// check if there's any data in persistent storage, will add to the store if any found
getData(store);

export default store;
