import AsyncStorage from '@react-native-community/async-storage';
import { addTodo } from '../actions';

const getData = async (store) => {
  try {
    const value = await AsyncStorage.getItem('todoStorage');
    if (value !== null) {
      const parsed = JSON.parse(value);
      parsed.forEach((item) => {
        store.dispatch(addTodo(item));
      });
    }
  } catch (e) {
    console.error(e);
  }
};

export default getData;
