import AsyncStorage from '@react-native-community/async-storage';

const storeData = async (value) => {
  try {
    const jsonValue = JSON.stringify(value);
    await AsyncStorage.setItem('todoStorage', jsonValue);
  } catch (e) {
    console.error(e);
  }
};

export default storeData;
